function init() {
    var photoUpload = document.getElementById('photoUpload');

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            if(user.uid === location.search.split("?id=")[1]){
                photoUpload.style.display = 'inline';
            }else{
                photoUpload.style.display = 'none';
            }
        } else {
            photoUpload.style.display = 'none';
        }
    });
    
    firebase.database().ref('/users/' + location.search.split("?id=")[1]).once('value').then(function(snapshot) {
        document.getElementById('card_img').setAttribute('src', snapshot.val().photoURL);
        document.getElementById('card_container').innerHTML = "<h4><b>" + snapshot.val().displayName + "</b></h4>";
    });
    
    var storageRef = firebase.storage().ref();
    var uploadFileInput = document.getElementById("uploadFileInput");
    uploadFileInput.addEventListener("change", function(){
        var file = this.files[0];
        var user = firebase.auth().currentUser;
        var uploadTask = storageRef.child('userPhotos/' + user.uid + "/" + file.name).put(file);
        uploadTask.on('state_changed', function(snapshot){
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED:

                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING:

                    console.log('Upload is running');
                    break;
            }
        }, function(error) {

        }, function() {
            var downloadURL = uploadTask.snapshot.downloadURL;
            console.log(downloadURL);
            user.updateProfile({
                photoURL: downloadURL
            }).then(function(){
                var updates = {};
                updates['users/' + user.uid + "/photoURL"] = user.photoURL;
                firebase.database().ref().update(updates).then(function() {
                    location.reload();
                }).catch(function(error){
                    console.log(error);
                });
            }, function(error) {
                console.log(error);
            });
        });
    },false);
}

window.onload = function () {
    init();
    initTopNav();    
};
