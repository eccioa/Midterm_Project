function init() {
     // Login with Email/Password
    var txtEmail = document.getElementById('txtEmail');
    var txtPassword = document.getElementById('txtPassword');

    var txtEmail2 = document.getElementById('txtEmail2');
    var txtPassword2 = document.getElementById('txtPassword2');

    var btnLogin = document.getElementById('btnLogin');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', e => {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(user) {
            location.href="index.html";
        }).catch(function(e){ 
            var login_error = document.getElementById('login_error');
            login_error.innerHTML = e.message;
            console.log(e.message)
        });
    });

    btnSignUp.addEventListener('click', e => {
        var email = txtEmail2.value;
        var password = txtPassword2.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user) {
            user.updateProfile({
                displayName: document.getElementById("userName").value,
                photoURL: "https://i.imgur.com/9DhTfKE.png"
            }).then(function () {
                console.log("Updated");
                firebase.database().ref('users/' + user.uid).set({
                    uid: user.uid,
                    displayName: user.displayName,
                    email: user.email,
                    photoURL: user.photoURL,
                }).then(function() {
                    location.href="index.html";
                }).catch(function(error){
                    console.error("寫入使用者資訊錯誤",error);
                });
            }, function (error) {
                console.log("Error happened");
            });

            
        }).catch(function(e){ 
            var signup_error = document.getElementById('signup_error');
            signup_error.innerHTML = e.message;
            console.log(e.message)
        });
    });

    function Logout(){
        firebase.auth().signOut();
    }

    // // Login with Google
    var btnLoginGooglePop = document.getElementById('btnLoginGooglePop');

    var provider = new firebase.auth.GoogleAuthProvider();

    btnLoginGooglePop.addEventListener('click', e => {
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            firebase.database().ref('users/' + user.uid).set({
                uid: user.uid,
                displayName: user.displayName,
                email: user.email,
                photoURL: user.photoURL,
            }).then(function() {
                location.href="index.html";
            }).catch(function(error){
                console.error("寫入使用者資訊錯誤",error);
            });
        }).catch(function (error) {
            login_error.innerHTML = error.message;
            console.log('error: ' + error.message);
        });
    });


    // // Login with Facebook
    // var btnLoginFBPop = document.getElementById('btnLoginFBPop');
    // var btnLoginFBRedi = document.getElementById('btnLoginFBRedi');

    // var provider = new firebase.auth.FacebookAuthProvider();

    // btnLoginFBPop.addEventListener('click', e => {
    //     firebase.auth().signInWithPopup(provider).then(function (result) {
    //         console.log('signInWithPopup');
    //         var token = result.credential.accessToken;
    //         var user = result.user;
    //     }).catch(function (error) {
    //         var errorCode = error.code;
    //         var errorMessage = error.message;
    //         var email = error.email;
    //         var credential = error.credential;
    //     });
    // });

    // btnLoginFBRedi.addEventListener('click', e => {
    //     firebase.auth().signInWithRedirect(provider);
    // });

    // // Manage Users
    // var usrDiv = document.getElementById('usrDiv');
    // var usrName = document.getElementById('usrName');
    // var useEmail = document.getElementById('useEmail');

    var logout_link = document.getElementById('logout_link');
    var login_link = document.getElementById('login_link');

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            // console.log(user);
        } else {
            console.log('not logged in');
        }
    });


    $('#signin').submit(function () {
        return false;
    });

    $('#signup').submit(function () {
        return false;
    });

}

window.onload = function () {
    init(); 
};