# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. User page
    2. Post page
    3. Post list page
    4. Leave comment under any post
* Other functions (add/delete)
    1. Change user photo
    2. Post list pagination
    3. Post time annotation
    4. "Page loading" animation
    5. Update when new comment added
    6. XSS prevention when adding new post and leaving comment

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

### Deployment

* [Click me](https://eccurom.cf/)
* Using custom domain instead of the default one

### Authentication

* Layout

<img src='https://i.imgur.com/S42AUyd.png'><br>
<img src='https://i.imgur.com/zLhfjGH.png'>

* Log in/Sign up in the same page using Bootstrap class
* Click the google label to log in with google
* User name required when signing up
* Error warning<br>
<img src='https://i.imgur.com/H6bl6Jo.png'>
* Automatically redirect to home page(post list page) after successfully Log in/Sign up
* Change the top nav after user log in
* Read/write database by authentication

### Post list page

* Display loading animation before the list come out<br>
<img src='https://i.imgur.com/pf9qgda.png'>
* Click on the post title to load the post page<br>
<img src='https://i.imgur.com/UHQcvat.png'>
* Pop-up window for adding new post<br>
<img src='https://i.imgur.com/Jc1r4Ez.png'>
* Clicking on the "post" button without login will be redirect to log in/sign up page

### Post page

* Layout
<img src='https://i.imgur.com/GiDyCEK.png'>
* Display loading animation before the content come out
* Display the author name, author photo, post title, post content and comments
* Click on the "Comment" button will scroll the page to bottom

### User page

* Layout
<img src='https://i.imgur.com/pKsU3HE.png'>
* Display user name and user photo
* Support for updating user photos by clicking the camera label
* CSS animation for the camera label


## Security Report (Optional)

* Read/write database by authentication
* XSS prevention when adding new post and leaving comment<br>
<img src='https://i.imgur.com/bHMUGrN.png'>
